/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author STDG_077
 */
public class Game {

    private Player player1, player2;
    private Table table;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void play() {
        boolean isFinish = false;
        printWelcome();
        newGame();
        while (!isFinish) {
            printTable();
            printTurn();
            inputRowCol();
            if (table.checkWin()) {
                printTable();
                printWinner();
                printPlayer();
                isFinish = true;
                break;
            }
            if (table.checkDraw()) {
                printTable();
                printDraw();
                printPlayer();
                isFinish = true;
                break;
            }
            table.switchPlayer();
        }

    }

    private void printWelcome() {
        System.out.println("Welcome to XO Game");
    }

    private void printTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println("");
        }
    }

    private void printTurn() {
        System.out.println("Player " + table.getCurrentPlayer().getSymbol() + " turn");
    }

    private void inputRowCol() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please input Number: ");
        int row = scanner.nextInt();
        int col = scanner.nextInt();
        table.setRowCol(row, col);

    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void printWinner() {
        System.out.println("Player " + table.getCurrentPlayer().getSymbol() + " Win!!!");
    }

    private void printDraw() {
        System.out.println("!! Game Draw !!");
    }
    
    private void printPlayer() {
        System.out.println(player1);
        System.out.println(player2);
    }
}
